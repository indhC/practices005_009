// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ArcanoidGameMode.generated.h"


UCLASS(MinimalAPI, Blueprintable, BlueprintType)
class AArcanoidGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AArcanoidGameMode();
};



