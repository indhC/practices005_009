// Copyright Epic Games, Inc. All Rights Reserved.

#include "ArcanoidGameMode.h"
#include "UObject/ConstructorHelpers.h"


AArcanoidGameMode::AArcanoidGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Practices5_9/Arcanoid/Framework/Characters/BP_ArcanoidCharacter"));
	if (nullptr != PlayerPawnBPClass.Class)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
