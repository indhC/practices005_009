﻿#pragma once

#include "Engine/DataTable.h"
#include "CommonGameConfigurations.generated.h"


///////////////////////////////////////////////////////////////////////////////////////////
/// Data Table Row Definitions
///////////////////////////////////////////////////////////////////////////////////////////

USTRUCT(BlueprintType)
struct FGameConfigurationTableRow: public FTableRowBase
{
    GENERATED_BODY()


    
};


USTRUCT(BlueprintType)
struct FLevelConfigurationTableRow: public FTableRowBase
{
    GENERATED_BODY()


    /** 3D configuration of the current Level which describes the Schema of each floor */
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FString> LevelSchemaNames;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float BlockWidth;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float BlockLength;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float BlockHeight;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float WidthGapSize;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float LengthGapSize;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float HeightGapSize;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 AdditionalLivesNumber;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString VolumeConfigurationName;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FString> NegativeEffectConfigurationNames;

};


USTRUCT(BlueprintType)
struct FBlockNamesCollection
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FString> BlockNames;

};


USTRUCT(BlueprintType)
struct FLevelSchemaConfigurationTableRow: public FTableRowBase
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FBlockNamesCollection> RowBlockNames;

};


USTRUCT(BlueprintType)
struct FVolumeConfigurationTableRow: public FTableRowBase
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UStaticMesh* VolumeMesh;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UMaterialInstance* VolumeMaterial;

    // TODO DOV: Other Volume parameters...

};

///////////////////////////////////////////////////////////////////////////////////////////
