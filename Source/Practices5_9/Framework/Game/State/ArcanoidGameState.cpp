// Fill out your copyright notice in the Description page of Project Settings.

#include "Practices5_9/Framework/Game/State/ArcanoidGameState.h"

#include "Practices5_9/Actors/Actions/AbstractItemAction.h"


UAbstractItemAction* AArcanoidGameState::GetAction(const TSubclassOf<UAbstractItemAction> InKey) const
{
    return (ActionsMapping.Contains(InKey)) ? (ActionsMapping[InKey]) : (nullptr);
}

UAbstractItemAction* AArcanoidGameState::AddAction(const TSubclassOf<UAbstractItemAction> InKey, UAbstractItemAction* InValue)
{
    return ActionsMapping.Add(InKey, InValue);
}
