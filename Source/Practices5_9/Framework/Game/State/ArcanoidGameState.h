// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "ArcanoidGameState.generated.h"


/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class PRACTICES5_9_API AArcanoidGameState : public AGameStateBase
{
	GENERATED_BODY()


	UPROPERTY()
	TMap<TSubclassOf<class UAbstractItemAction>, UAbstractItemAction*> ActionsMapping;


public:

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE UAbstractItemAction* GetAction(const TSubclassOf<UAbstractItemAction> InKey) const;
    
	UFUNCTION(BlueprintCallable)
	FORCEINLINE UAbstractItemAction* AddAction(const TSubclassOf<UAbstractItemAction> InKey, UAbstractItemAction* InValue);

};
