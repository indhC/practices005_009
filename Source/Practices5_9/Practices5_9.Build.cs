// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Practices5_9 : ModuleRules
{
	public Practices5_9(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
