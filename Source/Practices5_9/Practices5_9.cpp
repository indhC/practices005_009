// Copyright Epic Games, Inc. All Rights Reserved.

#include "Practices5_9.h"
#include "Modules/ModuleManager.h"


IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, Practices5_9, "Practices5_9");


DEFINE_LOG_CATEGORY(LogArcanoidGame);
