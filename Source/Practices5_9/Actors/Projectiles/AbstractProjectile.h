﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Practices5_9/Actors/AbstractItem.h"
#include "AbstractProjectile.generated.h"


UCLASS(Abstract, Blueprintable, BlueprintType)
class PRACTICES5_9_API AAbstractProjectile : public AAbstractItem
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "01. Movement Configuration", meta = (AllowPrivateAccess = "true"))
    float InitialSpeed;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "01. Movement Configuration", meta = (AllowPrivateAccess = "true"))
    float MinimalSpeed;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "01. Movement Configuration", meta = (AllowPrivateAccess = "true"))
    FVector InitialDirection;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "01. Movement Configuration", meta = (AllowPrivateAccess = "true"))
    float CollidingImpulseDecreasingPercents;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "02. Interaction", meta = (AllowPrivateAccess = "true"))
    int32 InteractionDamage;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Control", meta = (AllowPrivateAccess = "true"))
    bool bCanMove;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Control", meta = (AllowPrivateAccess = "true"))
    float Speed;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Control", meta = (AllowPrivateAccess = "true"))
    FVector Direction;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Control", meta = (AllowPrivateAccess = "true"))
    float CollidingImpulseDecreasingPart;

protected:

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:

    // Sets default values for this actor's properties
    AAbstractProjectile();


    virtual void OnConstruction(const FTransform& Transform) override;

    // Called every frame
    virtual void Tick(const float DeltaTime) override;


    ///
    /// Getters And Setters
    ///

    FORCEINLINE int32 GetInteractionDamage() const
    {
        return InteractionDamage;
    }

};
