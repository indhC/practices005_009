﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "AbstractProjectile.h"

#include "Practices5_9/Practices5_9.h"


namespace AbstractProjectile
{
    constexpr int32 GINDEX_DEFAULT_MATERIAL{0};
}


// Sets default values
AAbstractProjectile::AAbstractProjectile()
{
    UStaticMeshComponent* RootMesh{Cast<UStaticMeshComponent>(GetRootComponent())};
    RootMesh->SetSimulatePhysics(true);
    RootMesh->BodyInstance.bLockTranslation = true;
    RootMesh->BodyInstance.bLockZTranslation = true;

    bCanMove = true;
}

void AAbstractProjectile::OnConstruction(const FTransform& Transform)
{
    Super::OnConstruction(Transform);

    Speed = InitialSpeed;

    Direction = InitialDirection;
    Direction.Normalize();

    CollidingImpulseDecreasingPart = ArcanoidGame::GPERCENTAGE_MAX - ArcanoidGame::GPERCENTAGE_MULTIPLIER * CollidingImpulseDecreasingPercents;
}

// Called when the game starts or when spawned
void AAbstractProjectile::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void AAbstractProjectile::Tick(const float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (!bCanMove)
    {
        return;
    }

    FHitResult HitResult;
    AddActorWorldOffset(Direction * Speed * DeltaTime, true, &HitResult);

    if (!HitResult.GetComponent())
    {
        return;
    }

    Direction = FMath::GetReflectionVector(Direction, HitResult.Normal);
    Direction.Normalize();

    if (Speed > MinimalSpeed)
    {
        Speed = FMath::Max(Speed * CollidingImpulseDecreasingPart, MinimalSpeed);
    }
}
