﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "UObject/Object.h"
#include "ItemConfiguration.generated.h"


UENUM(BlueprintType)
enum class EItemActionDataKeys: uint8
{
    EIADK_CURRENT_HEALTH UMETA(DisplayName = "Item.Parameters.CurrentHealth"),
    EIADK_CURRENT_MATERIAL_INDEX UMETA(DisplayName = "Item.Parameters.CurrentMaterialIndex"),
};


USTRUCT(BlueprintType)
struct FItemConfigurationTableRow: public FTableRowBase
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<class AAbstractItem> ItemImplementationClass;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<class UAbstractItemAction> ActionImplementationClass;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 Health;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float BonusEffectChance;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float NegativeEffectChance;

};


USTRUCT(BlueprintType)
struct FItemActionData
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 IntegerData;


    static const FItemActionData EMPTY_DATA;

};


/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class PRACTICES5_9_API UItemConfiguration : public UObject
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Configuration", meta = (AllowPrivateAccess = "true"))
    bool bInitialized = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Configuration", meta = (AllowPrivateAccess = "true"))
    bool bAlive = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Configuration", meta = (AllowPrivateAccess = "true"))
    FItemConfigurationTableRow ItemTableData;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Configuration", meta = (AllowPrivateAccess = "true"))
    TArray<int32> MaterialParameters;

    // TODO DOV: We will not need this field at all for Instanced Mesh!
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Configuration", meta = (AllowPrivateAccess = "true"))
    TArray<UMaterialInstanceDynamic*> ItemDynamicMaterials;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Configuration", meta = (AllowPrivateAccess = "true"))
    UAbstractItemAction* ItemAction;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Configuration", meta = (AllowPrivateAccess = "true"))
    TMap<EItemActionDataKeys, FItemActionData> ActionsData;

public:

    UFUNCTION(BlueprintCallable)
    void Initialize(const bool bInInitialized, const bool bInAlive, const FItemConfigurationTableRow& InItemTableData, const TArray<int32>& InMaterialParameters);

    // TODO DOV: We will not need this field at all for Instanced Mesh!
    UFUNCTION(BlueprintCallable)
    void AddItemDynamicMaterials(UMaterialInstanceDynamic* InItemDynamicMaterial)
    {
        ItemDynamicMaterials.Add(InItemDynamicMaterial);
    }

    // TODO DOV: We will not need this field at all for Instanced Mesh!
    UFUNCTION(BlueprintCallable)
    void ClearItemDynamicMaterials()
    {
        ItemDynamicMaterials.Empty();
    }

    UFUNCTION(BlueprintCallable)
    bool ContainsActionData(const EItemActionDataKeys InKey) const
    {
        return ActionsData.Contains(InKey);
    }

    UFUNCTION(BlueprintCallable)
    const FItemActionData& GetActionData(const EItemActionDataKeys InKey) const
    {
        return (ActionsData.Contains(InKey)) ? (ActionsData[InKey]) : (FItemActionData::EMPTY_DATA);
    }

    UFUNCTION(BlueprintCallable)
    void AddActionData(const EItemActionDataKeys InKey, const FItemActionData& InActionsData)
    {
        (ActionsData.Contains(InKey)) ? (ActionsData[InKey] = InActionsData) : (ActionsData.Add(InKey, InActionsData));
    }


    ///
    /// Getters And Setters
    ///

    bool IsInitialized() const
    {
        return bInitialized;
    }

    void SetInitialized(const bool bInInitialized)
    {
        this->bInitialized = bInInitialized;
    }

    bool IsAlive() const
    {
        return bAlive;
    }

    void SetAlive(const bool bInAlive)
    {
        this->bAlive = bInAlive;
    }

    FItemConfigurationTableRow GetItemTableData() const
    {
        return ItemTableData;
    }

    void SetItemTableData(const FItemConfigurationTableRow& InItemTableData)
    {
        this->ItemTableData = InItemTableData;
    }

    TArray<int32> GetMaterialParameters() const
    {
        return MaterialParameters;
    }

    void SetMaterialParameters(const TArray<int32>& InMaterialParameters)
    {
        this->MaterialParameters = InMaterialParameters;
    }

    // TODO DOV: We will not need this field at all for Instanced Mesh!
    TArray<UMaterialInstanceDynamic*> GetItemDynamicMaterials() const
    {
        return ItemDynamicMaterials;
    }

    bool HasItemAction() const
    {
        return nullptr != ItemAction;
    }

    UAbstractItemAction* GetItemAction() const
    {
        return ItemAction;
    }

    void SetItemAction(UAbstractItemAction* InItemAction)
    {
        this->ItemAction = InItemAction;
    }

};
