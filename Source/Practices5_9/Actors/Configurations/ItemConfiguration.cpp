﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "ItemConfiguration.h"


const FItemActionData FItemActionData::EMPTY_DATA{};


void UItemConfiguration::Initialize(const bool bInInitialized, const bool bInAlive, const FItemConfigurationTableRow& InItemTableData,
                                    const TArray<int32>& InMaterialParameters)
{
    bInitialized = bInInitialized;
    bAlive = bInAlive;
    ItemTableData = InItemTableData;
    MaterialParameters = InMaterialParameters;
}
