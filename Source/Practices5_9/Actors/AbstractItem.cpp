﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "AbstractItem.h"

#include "Actions/ActionsRegistry.h"
#include "Practices5_9/Practices5_9.h"
#include "Practices5_9/Actors/Actions/MaterialsIteratingAction.h"


// Sets default values
AAbstractItem::AAbstractItem()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    ItemMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemMesh"));
    SetRootComponent(ItemMesh);
}

// Called when the game starts or when spawned
void AAbstractItem::BeginPlay()
{
    Super::BeginPlay();

    InitializeItem();
}

void AAbstractItem::InitializeItem()
{
    UItemConfiguration* Configuration{NewObject<UItemConfiguration>()};
    Configuration->Initialize(true, true, FItemConfigurationTableRow{}, TArray<int32>{});
    AddItem(FTransform{}, Configuration);

    if (ItemMaterialInitializingActionClass)
    {
        ResetItemMeshMaterials(ArcanoidGame::GINDEX_START);
        ItemMaterialInitializingAction = Cast<UMaterialsIteratingAction>(GetAction(ArcanoidGame::GINDEX_START, ItemMaterialInitializingActionClass));
    }
}

bool AAbstractItem::UpdateItemMeshDynamicMaterial(const int32 InIndex, const int32 InMaterialIndex, UMaterialInterface* InMaterial)
{
    UItemConfiguration* Configuration{FindItemConfiguration(InIndex)};
    if (!Configuration || !Configuration->IsAlive() || !Configuration->IsInitialized())
    {
        return false;
    }

    UMaterialInstanceDynamic* DynamicMaterialInstance{UMaterialInstanceDynamic::Create(InMaterial, this)};
    Configuration->AddItemDynamicMaterials(DynamicMaterialInstance);
    GetItemMesh()->SetMaterial(InMaterialIndex, DynamicMaterialInstance);
    return true;
}

UStaticMeshComponent* AAbstractItem::GetItemMesh() const
{
    return ItemMesh;
}

// Called every frame
void AAbstractItem::Tick(const float DeltaTime)
{
    Super::Tick(DeltaTime);
}

UAbstractItemAction* AAbstractItem::GetAction(const int32 InIndex, const TSubclassOf<UAbstractItemAction> InClassKey)
{
    return UActionsRegistry::GetAction(InIndex, this, InClassKey);
}

UItemConfiguration* AAbstractItem::FindItemConfiguration(const int32 InIndex) const
{
    return (GetItemMesh() && ItemsConfigurations.IsValidIndex(InIndex)) ? (ItemsConfigurations[InIndex]) : (nullptr);
}

bool AAbstractItem::RemoveItemConfiguration(const int32 InIndex)
{
    if(!ItemsConfigurations.IsValidIndex(InIndex))
    {
        return false;
    }

    ItemsConfigurations.RemoveAt(InIndex);
    return true;
}

bool AAbstractItem::IsAlive_Implementation(const int32 InIndex) const
{
    const UItemConfiguration* Configuration{FindItemConfiguration(InIndex)};
    if (!Configuration || !Configuration->IsAlive() || !Configuration->IsInitialized())
    {
        return false;
    }

    constexpr EItemActionDataKeys Key{EItemActionDataKeys::EIADK_CURRENT_HEALTH};
    return Configuration->ContainsActionData(Key) && (Configuration->GetActionData(Key).IntegerData > 0);
}

bool AAbstractItem::ResetItemMeshMaterials(const int32 InNewMaterialIndex, const int32 InIndex)
{
    PutItemActionData(InIndex, EItemActionDataKeys::EIADK_CURRENT_MATERIAL_INDEX, FItemActionData{InNewMaterialIndex});
    UItemConfiguration* Configuration{FindItemConfiguration(InIndex)};
    if (!Configuration || !Configuration->IsAlive() || !Configuration->IsInitialized())
    {
        return false;
    }

    Configuration->ClearItemDynamicMaterials();
    return true;
}

bool AAbstractItem::SetItemMeshMaterial(const int32 InIndex, const int32 InMaterialIndex, UMaterialInterface* InMaterial)
{
    return (nullptr != InMaterial) && UpdateItemMeshDynamicMaterial(InIndex, InMaterialIndex, InMaterial);
}

bool AAbstractItem::ContainsBlockActionData(const int32 InIndex, const EItemActionDataKeys DataKey) const
{
    const UItemConfiguration* Configuration{FindItemConfiguration(InIndex)};
    return Configuration && Configuration->IsAlive() && Configuration->IsInitialized() && Configuration->ContainsActionData(DataKey);
}

FItemActionData AAbstractItem::GetItemActionData(const int32 InIndex, const EItemActionDataKeys DataKey) const
{
    const UItemConfiguration* Configuration{FindItemConfiguration(InIndex)};
    return (Configuration && Configuration->IsAlive() && Configuration->IsInitialized()) ? (Configuration->GetActionData(DataKey)) : (FItemActionData{});
}

bool AAbstractItem::PutItemActionData(const int32 InIndex, const EItemActionDataKeys DataKey, const FItemActionData& Data)
{
    UItemConfiguration* Configuration{FindItemConfiguration(InIndex)};
    if (!Configuration || !Configuration->IsAlive() || !Configuration->IsInitialized())
    {
        return false;
    }

    Configuration->AddActionData(DataKey, Data);
    return true;
}

UAbstractItemAction* AAbstractItem::GetItemAction(const int32 InIndex)
{
    UItemConfiguration* Configuration{FindItemConfiguration(InIndex)};
    if (!Configuration || !Configuration->IsAlive() || !Configuration->IsInitialized())
    {
        return nullptr;
    }

    UAbstractItemAction* Action{Configuration->GetItemAction()};
    if (!Action && Configuration->GetItemTableData().ActionImplementationClass)
    {
        Action = GetAction(InIndex, Configuration->GetItemTableData().ActionImplementationClass);
        Configuration->SetItemAction(Action);
    }

    return Action;
}

FVector AAbstractItem::GetItemScale_Implementation(const int32 InIndex) const
{
    return (ItemMesh) ? (ItemMesh->GetRelativeScale3D()) : (FVector::ZeroVector);
}

bool AAbstractItem::SetItemScale_Implementation(const int32 InIndex, const FVector& InScale)
{
    if (ItemMesh)
    {
        ItemMesh->SetRelativeScale3D(InScale);
        return true;
    }

    return false;
}

int32 AAbstractItem::ClearItems_Implementation()
{
    ItemsConfigurations.Empty();
    return DestroyItem(ArcanoidGame::GINDEX_START);
}

bool AAbstractItem::AddItem_Implementation(const FTransform& InTransform, UItemConfiguration* InItemConfiguration)
{
    if (!InItemConfiguration)
    {
        return false;
    }

    const int32 Amount{ItemsConfigurations.Num()};
    return ItemsConfigurations.Add(InItemConfiguration) == Amount;
}

bool AAbstractItem::DestroyItem_Implementation(const int32 InIndex)
{
    return Destroy();
}
