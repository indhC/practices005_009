﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Configurations/ItemConfiguration.h"
#include "AbstractItem.generated.h"


UCLASS(Abstract, Blueprintable, BlueprintType)
class PRACTICES5_9_API AAbstractItem : public AActor
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "00. Item Configuration", meta = (AllowPrivateAccess = "true"))
    UStaticMeshComponent* ItemMesh;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "00. Item Configuration", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<class UMaterialsIteratingAction> ItemMaterialInitializingActionClass;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Control", meta = (AllowPrivateAccess = "true"))
    TArray<UItemConfiguration*> ItemsConfigurations;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Control", meta = (AllowPrivateAccess = "true"))
    UMaterialsIteratingAction* ItemMaterialInitializingAction;

protected:

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;


    UFUNCTION()
    virtual void InitializeItem();

    UFUNCTION()
    // TODO DOV: This method MUST BE rewritten to update Material parameter (CustomSMData value!!!) instead of the whole Material!
    // TODO DOV: This means that it will need ItemIndex...
    bool UpdateItemMeshDynamicMaterial(const int32 InIndex, const int32 InMaterialIndex, UMaterialInterface* InMaterial);


    ///
    /// Getters And Setters
    ///

    UFUNCTION()
    virtual UStaticMeshComponent* GetItemMesh() const;

public:

    // Sets default values for this actor's properties
    AAbstractItem();


    // Called every frame
    virtual void Tick(const float DeltaTime) override;


    UFUNCTION(BlueprintCallable, BlueprintPure)
    FORCEINLINE UAbstractItemAction* GetAction(const int32 InIndex, const TSubclassOf<UAbstractItemAction> InClassKey);

    UFUNCTION(BlueprintCallable)
    UItemConfiguration* FindItemConfiguration(const int32 InIndex) const;

    UFUNCTION(BlueprintCallable)
    bool RemoveItemConfiguration(const int32 InIndex);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, BlueprintPure)
    FORCEINLINE bool IsAlive(const int32 InIndex) const;

    // TODO DOV: This method MUST BE rewritten to update Material parameter (CustomSMData value!!!) instead of the whole Material!
    // TODO DOV: This means that it will need ItemIndex...
    UFUNCTION(BlueprintCallable)
    FORCEINLINE bool ResetItemMeshMaterials(const int32 InNewMaterialIndex, const int32 InIndex = 0);

    // TODO DOV: This method MUST BE rewritten to update Material parameter (CustomSMData value!!!) instead of the whole Material!
    // TODO DOV: This means that it will need ItemIndex...
    UFUNCTION(BlueprintCallable)
    FORCEINLINE bool SetItemMeshMaterial(const int32 InIndex, const int32 InMaterialIndex, UMaterialInterface* InMaterial);

    UFUNCTION(BlueprintCallable, BlueprintPure)
    FORCEINLINE bool ContainsBlockActionData(const int32 InIndex, const EItemActionDataKeys DataKey) const;

    UFUNCTION(BlueprintCallable, BlueprintPure)
    FORCEINLINE FItemActionData GetItemActionData(const int32 InIndex, const EItemActionDataKeys DataKey) const;

    UFUNCTION(BlueprintCallable)
    FORCEINLINE bool PutItemActionData(const int32 InIndex, const EItemActionDataKeys DataKey, const FItemActionData& Data);

    UFUNCTION(BlueprintCallable)
    FORCEINLINE UAbstractItemAction* GetItemAction(const int32 InIndex);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    FORCEINLINE FVector GetItemScale(const int32 InIndex) const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    FORCEINLINE bool SetItemScale(const int32 InItemIndex, const FVector& InScale);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    int32 ClearItems();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    bool AddItem(const FTransform& InTransform, UItemConfiguration* InItemConfiguration);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    bool DestroyItem(const int32 InIndex);

};
