// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractItemAction.generated.h"


/**
 * 
 */
UCLASS(Abstract, Blueprintable, BlueprintType)
class PRACTICES5_9_API UAbstractItemAction : public UObject
{
	GENERATED_BODY()


    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Control", meta = (AllowPrivateAccess = "true"))
    bool bConfigurationValid;

protected:

    ///
    /// Getters And Setters
    ///

    FORCEINLINE void SetConfigurationValid(const bool bInValue)
    {
        bConfigurationValid = bInValue;
    }

public:

    UAbstractItemAction();


    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void Initialize(const int32 InIndex, class AAbstractItem* InItem);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void InitializeItem(const int32 InIndex, AAbstractItem* InItem);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	bool Execute(const int32 InIndex, AAbstractItem* InItem, const class AAbstractProjectile* InProjectile);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    bool DoExecution(const int32 InIndex, AAbstractItem* InItem, const AAbstractProjectile* InProjectile);

};
