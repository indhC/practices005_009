// Fill out your copyright notice in the Description page of Project Settings.

#include "Practices5_9/Actors/Actions/ActionsRegistry.h"

#include "AbstractItemAction.h"
#include "Kismet/GameplayStatics.h"
#include "Practices5_9/Actors/AbstractItem.h"
#include "Practices5_9/Framework/Game/State/ArcanoidGameState.h"


UAbstractItemAction* UActionsRegistry::GetAction(const int32 InIndex, AAbstractItem* InBlock, const TSubclassOf<UAbstractItemAction> InClassKey)
{
    if (!InBlock || !InClassKey)
    {
        return nullptr;
    }

    AArcanoidGameState* GameState{Cast<AArcanoidGameState>(UGameplayStatics::GetGameState(InBlock))};
    if (!GameState)
    {
        return nullptr;
    }

    UAbstractItemAction* Result{GameState->GetAction(InClassKey)};
    if (!Result)
    {
        Result = NewObject<UAbstractItemAction>(InBlock, InClassKey);
        Result->Initialize(InIndex, InBlock);
        GameState->AddAction(InClassKey, Result);
    }

    if (Result)
    {
        Result->InitializeItem(InIndex, InBlock);
    }

    return Result;
}
