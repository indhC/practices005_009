﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractItemAction.h"
#include "ScaleReductionAction.generated.h"


/**
 * 
 */
UCLASS()
class PRACTICES5_9_API UScaleReductionAction : public UAbstractItemAction
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "01. Interaction", meta = (AllowPrivateAccess = "true"))
    float ScaleReductionPercentsMin;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "01. Interaction", meta = (AllowPrivateAccess = "true"))
    float ScaleReductionPercents;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Control", meta = (AllowPrivateAccess = "true"))
    float ScaleReductionPartMin;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Control", meta = (AllowPrivateAccess = "true"))
    float ScaleReductionPart;

public:

    virtual void Initialize_Implementation(const int32 InIndex, AAbstractItem* InItem) override;

    virtual bool DoExecution_Implementation(const int32 InIndex, AAbstractItem* InItem, const AAbstractProjectile* InProjectile) override;

};
