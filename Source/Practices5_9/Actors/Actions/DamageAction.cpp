﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "DamageAction.h"

#include "Practices5_9/Actors/AbstractItem.h"
#include "Practices5_9/Actors/Projectiles/AbstractProjectile.h"


bool UDamageAction::DoExecution_Implementation(const int32 InIndex, AAbstractItem* InItem, const AAbstractProjectile* InProjectile)
{
    if (!InProjectile || !Super::DoExecution_Implementation(InIndex, InItem, InProjectile)
        || !InItem->ContainsBlockActionData(InIndex, EItemActionDataKeys::EIADK_CURRENT_HEALTH))
    {
        return false;
    }

    const FItemActionData CurrentHealthData{InItem->GetItemActionData(InIndex, EItemActionDataKeys::EIADK_CURRENT_HEALTH)};
    const int32 Damage{InProjectile->GetInteractionDamage()};
    if(!InItem->PutItemActionData(InIndex, EItemActionDataKeys::EIADK_CURRENT_HEALTH, FItemActionData{CurrentHealthData.IntegerData - Damage}))
    {
        return false;
    }

    const bool bResult{CurrentHealthData.IntegerData > Damage};
    if (!bResult)
    {
        InItem->DestroyItem(InIndex);
    }

    return bResult;
}
