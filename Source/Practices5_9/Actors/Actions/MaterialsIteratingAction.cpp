﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "MaterialsIteratingAction.h"

#include "Practices5_9/Practices5_9.h"
#include "Practices5_9/Actors/AbstractItem.h"


UMaterialsIteratingAction::UMaterialsIteratingAction()
{
    bRepeatingIteratingInfinitely = false;
}

void UMaterialsIteratingAction::Initialize_Implementation(const int32 InIndex, AAbstractItem* InItem)
{
    Super::Initialize_Implementation(InIndex, InItem);

    if (Materials.Num() % MaterialIndexes.Num())
    {
        SetConfigurationValid(false);
    }
}

bool UMaterialsIteratingAction::DoExecution_Implementation(const int32 InIndex, AAbstractItem* InItem, const AAbstractProjectile* InProjectile)
{
    if (!Super::DoExecution_Implementation(InIndex, InItem, InProjectile))
    {
        return false;
    }

    constexpr EItemActionDataKeys IndexKey{EItemActionDataKeys::EIADK_CURRENT_MATERIAL_INDEX};
    int32 MaterialIndex{(InItem->ContainsBlockActionData(InIndex, IndexKey)) ? (InItem->GetItemActionData(InIndex, IndexKey).IntegerData) : (ArcanoidGame::GINDEX_START)};
    if (!bRepeatingIteratingInfinitely && (MaterialIndex >= Materials.Num()))
    {
        return true;
    }

    int32 FinalMaterialIndex{MaterialIndexes.Num() + MaterialIndex};
    if (bRepeatingIteratingInfinitely && (FinalMaterialIndex >= Materials.Num()))
    {
        FinalMaterialIndex = ArcanoidGame::GINDEX_START;
    }

    InItem->ResetItemMeshMaterials(FinalMaterialIndex, InIndex);
    for (const int32 Index : MaterialIndexes)
    {
        InItem->SetItemMeshMaterial(InIndex, Index, Materials[MaterialIndex++]);
    }

    return true;
}
