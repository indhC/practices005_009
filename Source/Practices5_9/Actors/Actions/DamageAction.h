﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractItemAction.h"
#include "DamageAction.generated.h"


/**
 * 
 */
UCLASS()
class PRACTICES5_9_API UDamageAction : public UAbstractItemAction
{
    GENERATED_BODY()

public:

    virtual bool DoExecution_Implementation(const int32 InIndex, AAbstractItem* InItem, const AAbstractProjectile* InProjectile) override;

};
