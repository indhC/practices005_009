﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractItemAction.h"
#include "CompositeBlockAction.generated.h"


/**
 * 
 */
UCLASS()
class PRACTICES5_9_API UCompositeBlockAction : public UAbstractItemAction
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "01. Interaction", meta = (AllowPrivateAccess = "true"))
    TArray<TSubclassOf<UAbstractItemAction>> ActionClasses;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Control", meta = (AllowPrivateAccess = "true"))
    TArray<UAbstractItemAction*> Actions;

public:

    UCompositeBlockAction();


    virtual void Initialize_Implementation(const int32 InIndex, AAbstractItem* InItem) override;

    virtual bool DoExecution_Implementation(const int32 InIndex, AAbstractItem* InItem, const AAbstractProjectile* InProjectile) override;

};
