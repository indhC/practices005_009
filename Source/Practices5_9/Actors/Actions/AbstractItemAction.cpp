// Fill out your copyright notice in the Description page of Project Settings.

#include "Practices5_9/Actors/Actions/AbstractItemAction.h"

#include "Practices5_9/Practices5_9.h"
#include "Practices5_9/Actors/AbstractItem.h"


UAbstractItemAction::UAbstractItemAction()
{
    bConfigurationValid = true;
}

// ReSharper disable once CppMemberFunctionMayBeStatic
void UAbstractItemAction::Initialize_Implementation(const int32 InIndex, AAbstractItem* InItem)
{
}

void UAbstractItemAction::InitializeItem_Implementation(const int32 InIndex, AAbstractItem* InItem)
{
}

bool UAbstractItemAction::Execute_Implementation(const int32 InIndex, AAbstractItem* InItem, const AAbstractProjectile* InProjectile)
{
    // This Interaction Action cannot be executed normally due to a misconfiguration, but the remaining Actions can be executed normally
    if (!bConfigurationValid)
    {
        UE_LOG(LogArcanoidGame, Warning, TEXT("The '%s' Interaction Action has an invalid configuration and cannot work properly!"), *GetClass()->GetFullName());
        return true;
    }

    if (!InItem || !InItem->IsAlive(InIndex))
    {
        return false;
    }

    return DoExecution(InIndex, InItem, InProjectile);
}

bool UAbstractItemAction::DoExecution_Implementation(const int32 InIndex, AAbstractItem* InItem, const AAbstractProjectile* InProjectile)
{
    return true;
}
