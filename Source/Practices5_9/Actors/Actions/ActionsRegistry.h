// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ActionsRegistry.generated.h"


/**
 * 
 */
UCLASS()
class PRACTICES5_9_API UActionsRegistry : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

    UFUNCTION(BlueprintCallable, BlueprintPure)
    static UAbstractItemAction* GetAction(const int32 InIndex, AAbstractItem* InBlock, const TSubclassOf<UAbstractItemAction> InClassKey);

};
