﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractItemAction.h"
#include "MaterialsIteratingAction.generated.h"


/**
 * TODO: It is necessary to change the logic to iterate between material functions of the material!
 */
UCLASS()
class PRACTICES5_9_API UMaterialsIteratingAction : public UAbstractItemAction
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "01. Interaction", meta = (AllowPrivateAccess = "true"))
    TArray<int32> MaterialIndexes;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "01. Interaction", meta = (AllowPrivateAccess = "true"))
    TArray<UMaterialInterface*> Materials;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "01. Interaction", meta = (AllowPrivateAccess = "true"))
    bool bRepeatingIteratingInfinitely;

public:

    UMaterialsIteratingAction();


    virtual void Initialize_Implementation(const int32 InIndex, AAbstractItem* InItem) override;

    /**
     * @brief This implementation is based on the following rules:
     * 1. If Repeating is false, then the Block parameter for this Action EItemActionDataKeys::EIADK_CURRENT_MATERIAL_INDEX will be equal to or greater than the number of
     * Materials;
     * 2. If Repeating is set to true, then the Block parameter for this Action EItemActionDataKeys::EIADK_CURRENT_MATERIAL_INDEX will always reset to
     * ArcanoidGame::GINDEX_START when using the last Material on the some iteration.
     * @param InIndex int32 value of the Block Instance index
     * @param InItem AAbstractBlock* instance of the current Block
     * @param InProjectile possible AAbstractProjectile* instance of the Projectile by which hit the specified Block
     * @return bool value which indicates that the current Block Action execution found no problems for the execution of the remaining Actions
     */
    virtual bool DoExecution_Implementation(const int32 InIndex, AAbstractItem* InItem, const AAbstractProjectile* InProjectile) override;

};
