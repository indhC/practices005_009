﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "CompositeBlockAction.h"

#include "Practices5_9/Practices5_9.h"
#include "Practices5_9/Actors/AbstractItem.h"


UCompositeBlockAction::UCompositeBlockAction()
{
    SetConfigurationValid(false);
}

void UCompositeBlockAction::Initialize_Implementation(const int32 InIndex, AAbstractItem* InItem)
{
    Super::Initialize_Implementation(InIndex, InItem);

    if (!ActionClasses.IsValidIndex(ArcanoidGame::GINDEX_START))
    {
        return;
    }

    for (const TSubclassOf<UAbstractItemAction>& Class : ActionClasses)
    {
        if (!Class)
        {
            continue;
        }

        SetConfigurationValid(true);
        Actions.Add(InItem->GetAction(InIndex, Class));
    }
}

bool UCompositeBlockAction::DoExecution_Implementation(const int32 InIndex, AAbstractItem* InItem, const AAbstractProjectile* InProjectile)
{
    if (!Super::DoExecution_Implementation(InIndex, InItem, InProjectile))
    {
        return false;
    }

    for (UAbstractItemAction* Action : Actions)
    {
        if (!Action->Execute(InIndex, InItem, InProjectile))
        {
            return false;
        }
    }

    return true;
}
