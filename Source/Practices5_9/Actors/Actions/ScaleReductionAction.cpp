﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "ScaleReductionAction.h"

#include "Practices5_9/Practices5_9.h"
#include "Practices5_9/Actors/AbstractItem.h"


void UScaleReductionAction::Initialize_Implementation(const int32 InIndex, AAbstractItem* InItem)
{
    Super::Initialize_Implementation(InIndex, InItem);

    ScaleReductionPartMin = ArcanoidGame::GPERCENTAGE_MULTIPLIER * ScaleReductionPercentsMin;
    ScaleReductionPart = ArcanoidGame::GPERCENTAGE_MAX - ArcanoidGame::GPERCENTAGE_MULTIPLIER * ScaleReductionPercents;
}

bool UScaleReductionAction::DoExecution_Implementation(const int32 InIndex, AAbstractItem* InItem, const AAbstractProjectile* InProjectile)
{
    if (!Super::DoExecution_Implementation(InIndex, InItem, InProjectile))
    {
        return false;
    }

    const FVector CurrentScale{InItem->GetItemScale(InIndex)};
    if (CurrentScale.X > ScaleReductionPartMin)
    {
        const float UpdatedScaleValue{FMath::Max(CurrentScale.X * ScaleReductionPart, ScaleReductionPart)};
        InItem->SetItemScale(InIndex, FVector{UpdatedScaleValue});
    }

    return true;
}
