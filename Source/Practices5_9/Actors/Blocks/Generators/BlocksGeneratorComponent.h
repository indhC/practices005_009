﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BlocksGeneratorComponent.generated.h"


UCLASS(Blueprintable, BlueprintType)
class PRACTICES5_9_API UBlocksGeneratorComponent : public UActorComponent
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "00. Generator Tables Configuration", meta = (AllowPrivateAccess = "true"))
    class UDataTable* BlocksConfigurationTable;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "00. Generator Tables Configuration", meta = (AllowPrivateAccess = "true"))
    UDataTable* LevelSchemasConfigurationTable;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "00. Generator Tables Configuration", meta = (AllowPrivateAccess = "true"))
    UDataTable* LevelsConfigurationTable;;

protected:

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;


    UFUNCTION()
    virtual void LogTablesConfigurationWarning() const;

public:

    // Sets default values for this actor's properties
    UBlocksGeneratorComponent();


    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    int32 Generate(class AAbstractBlock* InBlock);

};
