﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "BlocksGeneratorComponent.h"

#include "Practices5_9/Practices5_9.h"


// Sets default values
UBlocksGeneratorComponent::UBlocksGeneratorComponent()
{
}

// Called when the game starts or when spawned
void UBlocksGeneratorComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UBlocksGeneratorComponent::LogTablesConfigurationWarning() const
{
    UE_LOG(LogArcanoidGame, Warning, TEXT("The Block Generator has an invalid data tables configuration and cannot work properly!"));
    UE_LOG(LogArcanoidGame, Warning, TEXT("\tBlocksConfigurationTable is NULL: %d"), nullptr == BlocksConfigurationTable);
    UE_LOG(LogArcanoidGame, Warning, TEXT("\tLevelSchemasConfigurationTable is NULL: %d"), nullptr == LevelSchemasConfigurationTable);
    UE_LOG(LogArcanoidGame, Warning, TEXT("\tLevelsConfigurationTable is NULL: %d"), nullptr == LevelsConfigurationTable);
}

int32 UBlocksGeneratorComponent::Generate_Implementation(AAbstractBlock* InBlock)
{
    if(!BlocksConfigurationTable || !LevelSchemasConfigurationTable || !LevelsConfigurationTable)
    {
        LogTablesConfigurationWarning();
        return ArcanoidGame::GAMOUNT_INVALID_CONFIGURATION;
    }

    return 0;
}
