﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "AbstractBlock.h"

#include "Generators/BlocksGeneratorComponent.h"
#include "Practices5_9/Actors/Actions/AbstractItemAction.h"
#include "Practices5_9/Actors/Configurations/ItemConfiguration.h"
#include "Practices5_9/Actors/Projectiles/AbstractProjectile.h"


namespace AbstractBlock
{
}


// Sets default values
AAbstractBlock::AAbstractBlock(): AAbstractItem()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    BlockMesh = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("BlockMesh"));
    BlockMesh->NumCustomDataFloats = static_cast<int32>(EBlockCustomDataIndex::EBCDI_COUNT);
    SetRootComponent(BlockMesh);

    Super::GetItemMesh()->DestroyComponent();

    BlocksGeneratorComponent = CreateDefaultSubobject<UBlocksGeneratorComponent>(TEXT("BlocksGeneratorComponent"));
}

void AAbstractBlock::OnConstruction(const FTransform& Transform)
{
    Super::OnConstruction(Transform);

    BlockMesh->NumCustomDataFloats = static_cast<int32>(EBlockCustomDataIndex::EBCDI_COUNT);

    if (BlocksGeneratorComponentClass)
    {
        BlocksGeneratorComponent = NewObject<UBlocksGeneratorComponent>(this, BlocksGeneratorComponentClass);
    }
}

// Called when the game starts or when spawned
void AAbstractBlock::BeginPlay()
{
    Super::BeginPlay();

    Cast<UStaticMeshComponent>(GetRootComponent())->OnComponentHit.AddDynamic(this, &AAbstractBlock::OnBlockMeshComponentHit);

    if (BlocksGeneratorComponent)
    {
        BlocksGeneratorComponent->Generate(this);
    }
}

void AAbstractBlock::InitializeItem()
{
}

UStaticMeshComponent* AAbstractBlock::GetItemMesh() const
{
    return BlockMesh;
}

// ReSharper disable once CppMemberFunctionMayBeConst
void AAbstractBlock::OnBlockMeshComponentHit(
    UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
    if (nullptr == Hit.GetActor())
    {
        return;
    }

    const AAbstractProjectile* Projectile{Cast<AAbstractProjectile>(OtherActor)};
    if (!Projectile)
    {
        return;
    }

    if (UAbstractItemAction* ItemAction{GetItemAction(Hit.Item)})
    {
        ItemAction->Execute(Hit.Item, this, Projectile);
    }
}

// Called every frame
void AAbstractBlock::Tick(const float DeltaTime)
{
    Super::Tick(DeltaTime);
}

FVector AAbstractBlock::GetItemScale_Implementation(const int32 InIndex) const
{
    if (!BlockMesh->PerInstanceSMData.IsValidIndex(InIndex))
    {
        return FVector::ZeroVector;
    }

    FTransform Transform;
    BlockMesh->GetInstanceTransform(InIndex, Transform);
    return Transform.GetScale3D();
}

bool AAbstractBlock::SetItemScale_Implementation(const int32 InIndex, const FVector& InScale)
{
    if (!BlockMesh->PerInstanceSMData.IsValidIndex(InIndex))
    {
        return false;
    }

    FTransform Transform;
    BlockMesh->GetInstanceTransform(InIndex, Transform);
    Transform.SetScale3D(InScale);
    return BlockMesh->UpdateInstanceTransform(InIndex, Transform, false, true);
}

int32 AAbstractBlock::ClearItems_Implementation()
{
    const int32 Result{BlockMesh->GetInstanceCount()};
    BlockMesh->ClearInstances();
    return Result;
}

bool AAbstractBlock::AddItem_Implementation(const FTransform& InTransform, UItemConfiguration* InItemConfiguration)
{
    if (!Super::AddItem_Implementation(InTransform, InItemConfiguration))
    {
        return false;
    }

    const int32 NewIndex{BlockMesh->AddInstance(InTransform)};
    PutItemActionData(NewIndex, EItemActionDataKeys::EIADK_CURRENT_HEALTH, FItemActionData{InItemConfiguration->GetItemTableData().Health});

    // TODO DOV: Fill Custom SM Data Here!!!
    return true;
}

bool AAbstractBlock::DestroyItem_Implementation(const int32 InIndex)
{
    RemoveItemConfiguration(InIndex);
    if (BlockMesh->PerInstanceSMData.IsValidIndex(InIndex))
    {
        return BlockMesh->RemoveInstance(InIndex);
    }

    return false;
}
