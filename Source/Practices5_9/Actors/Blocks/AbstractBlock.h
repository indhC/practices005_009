﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Practices5_9/Actors/AbstractItem.h"
#include "AbstractBlock.generated.h"


UENUM(BlueprintType)
enum class EBlockCustomDataIndex: uint8
{
    EBCDI_COUNT UMETA(DisplayName = "CustomData.Count")
};


UCLASS(Abstract, Blueprintable, BlueprintType)
class PRACTICES5_9_API AAbstractBlock : public AAbstractItem
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "01. Block Parameters", meta = (AllowPrivateAccess = "true"))
    UInstancedStaticMeshComponent* BlockMesh;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "01. Block Parameters", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<class UBlocksGeneratorComponent> BlocksGeneratorComponentClass;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "01. Block Parameters", meta = (AllowPrivateAccess = "true"))
    UBlocksGeneratorComponent* BlocksGeneratorComponent;

protected:

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    virtual void InitializeItem() override;

    virtual UStaticMeshComponent* GetItemMesh() const override;


    UFUNCTION()
    void OnBlockMeshComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

public:

    // Sets default values for this actor's properties
    AAbstractBlock();

    virtual void OnConstruction(const FTransform& Transform) override;


    // Called every frame
    virtual void Tick(const float DeltaTime) override;


    virtual FVector GetItemScale_Implementation(const int32 InIndex) const override;

    virtual bool SetItemScale_Implementation(const int32 InIndex, const FVector& InScale) override;

    virtual int32 ClearItems_Implementation() override;

    virtual bool AddItem_Implementation(const FTransform& InTransform, UItemConfiguration* InItemConfiguration) override;

    virtual bool DestroyItem_Implementation(const int32 InIndex) override;

};
