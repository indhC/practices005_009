﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "UObject/Object.h"
#include "AbstractEffect.generated.h"


USTRUCT(BlueprintType)
struct FEffectConfigurationTableRow: public FTableRowBase
{
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<class UAbstractEffect> EffectImplementationClass;

    // TODO DOV: Other Effect parameters...

};


/**
 * 
 */
UCLASS()
class PRACTICES5_9_API UAbstractEffect : public UObject
{
    GENERATED_BODY()
    
};
