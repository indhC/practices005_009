﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Practices5_9/Actors/AbstractItem.h"
#include "AbstractPlatform.generated.h"


UCLASS()
class PRACTICES5_9_API AAbstractPlatform : public AAbstractItem
{
    GENERATED_BODY()

protected:

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:

    // Sets default values for this actor's properties
    AAbstractPlatform();


    // Called every frame
    virtual void Tick(const float DeltaTime) override;

};
