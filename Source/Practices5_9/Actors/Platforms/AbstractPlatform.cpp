﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "AbstractPlatform.h"


// Sets default values
AAbstractPlatform::AAbstractPlatform()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AAbstractPlatform::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void AAbstractPlatform::Tick(const float DeltaTime)
{
    Super::Tick(DeltaTime);
}
